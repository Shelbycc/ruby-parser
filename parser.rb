require 'open-uri'
require 'nokogiri'
require 'csv'

CSV.open(ARGV[1], "w") do |csv|
  url = ARGV[0]
  html = open(url)

  doc = Nokogiri::HTML(html)
  products_count = doc.xpath("//small[@class='heading-counter']/text()").to_s.split(" ")[1].to_i
  pages_count = products_count/20.0
  pages_count = pages_count.ceil

  links = []

  for i in 1..pages_count
    url = ARGV[0] + "?p=#{i}"
    html = open(url)
    doc = Nokogiri::HTML(html)
    products = doc.xpath("//a[@class='product-name']/@href")
    links += products
  end

  links.to_a.each do |url|
    doc = Nokogiri::HTML(open(url))
    name = doc.xpath("//h1[@class='nombre_producto']/text()").to_s.strip
    weights = doc.xpath("//ul[@class='attribute_labels_lists']//span[@class='attribute_name']/text()")
    prices = doc.xpath("//ul[@class='attribute_labels_lists']//span[@class='attribute_price']/text()")
    image = doc.xpath("//img[@id='bigpic']/@src")
 
    weights.to_a.each_index do |i|
      weight = weights[i].to_s.strip
      price = prices[i].to_s.strip
      csv << [name + " - " + weight, price, image]
    end
  end
end

